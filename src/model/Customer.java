package model;

import businessLayer.CustomerValidator;

public class Customer {
	//for simplicity, we describe all fields as strings. 
	//There's no need to describe them as anything else anyway
	
	private int id;
	private String name, address;
	
	public Customer(){
		this.setId(0);
		this.setName("John Doe");
		this.setAddress("Strada Fericirii, Nr.48, ap. 18");
	}		
	
	public Customer(int id, String name, String address){
		CustomerValidator.validate(name, address);
		this.setId(id);
		this.setName(name);
		this.setAddress(address);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
