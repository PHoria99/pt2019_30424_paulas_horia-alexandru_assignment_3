package model;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import businessLayer.OrderValidator;

public class Order {
	
	private int id;
	private String buyer, product;
	private String quantity;
	
	public Order() {
		this.setId(0);
		this.setBuyer("John Doe");
		this.setProduct("Rosii");
		this.setQuantity("10");
	}
	
	public Order(int id, String buyer, String product, String quantity) {
		this.setId(id);
		this.setBuyer(buyer);
		this.setProduct(product);
		OrderValidator.validate(quantity);
		this.setQuantity(quantity);
		this.generateTicket();
	}
	
	private void generateTicket()
	{
			BufferedWriter writer = null;
		        try {
		            //create a temporary file
		            String fileName = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		        	String timeLog = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(Calendar.getInstance().getTime());
		            File logFile = new File(fileName);

		            // This will output the full path where the file will be written to...
		            System.out.println(logFile.getCanonicalPath());

		            writer = new BufferedWriter(new FileWriter(logFile));
		            writer.write(this.buyer + " Bought:\n" + this.product + " X " + this.quantity + "\n" + timeLog);
		        } catch (Exception e) {
		            e.printStackTrace();
		        } finally {
		            try {
		                // Close the writer regardless of what happens...
		                writer.close();
		            } catch (Exception e) {
		            }}
	}

	public String getBuyer() {
		return buyer;
	}

	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
