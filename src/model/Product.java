package model;

import businessLayer.ProductValidator;

public class Product {
	
	private int id;
	private String name, price, quantity;

	public Product() {
		this.setID(0);
		this.setName("Rosii");
		this.setPrice("10");
		this.setQuantity("100");
	}

	public Product(int id, String name, String price, String quantity) {
		ProductValidator.validate(name, quantity, price);
		this.setID(id);
		this.setName(name);
		this.setPrice(price);
		this.setQuantity(quantity);
	}

	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

}
