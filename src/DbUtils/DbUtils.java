package DbUtils;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class DbUtils {
    public static void resultSetToTableModel(JTable table, ResultSet rs) {
	try {
	    ResultSetMetaData metaData = rs.getMetaData();
	    int numberOfColumns = metaData.getColumnCount();
	    Vector<String> columnNames = new Vector<String>();

	    // Get the column names
	    for (int column = 1; column <= numberOfColumns; column++) {
	    	columnNames.addElement(metaData.getColumnLabel(column));
	    }

	    // Get all rows.
	    Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
	    
	    while (rs.next()){
		    Vector<Object> newRow = new Vector<Object>();
			for (int i = 1; i <= numberOfColumns; i++) {
			    newRow.addElement(rs.getObject(i));
			}
	
			rows.addElement(newRow);
		} 
	    
	    table.setModel(new DefaultTableModel(rows, columnNames));
	} catch (Exception e) {
	    e.printStackTrace();

	}
    }
}
