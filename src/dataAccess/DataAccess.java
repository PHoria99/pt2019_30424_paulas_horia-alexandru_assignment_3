package dataAccess;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;

import DbUtils.DbUtils;
import businessLayer.*;


@SuppressWarnings("unused")
public class DataAccess {
	
	public Connection connection;
	public Statement ps;
	public ResultSet rs;
	
	public DataAccess() throws SQLException{
		connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/store", "admin", "Pass123$");
		ps = connection.createStatement();
	}
	
	public void insertElement(Object object) throws SQLException{
		StringBuilder stringBuild = new StringBuilder("INSERT INTO `store`.`");
		String classType = object.getClass().getSimpleName();
		Field[] fields = object.getClass().getDeclaredFields();
		
		stringBuild.append(classType.toLowerCase());
		stringBuild.append("` (");
		
		/**Create the insertion statement using reflection*/
		for(int i = 0; i < fields.length; i++) {
			stringBuild.append("`");
			stringBuild.append(fields[i].getName());
			if(i+1<fields.length) stringBuild.append("`,");
			else stringBuild.append("`) VALUES (");
		}
		for(int i = 0; i < fields.length; i++) {
			stringBuild.append("'");
			Object value = null;
			//Use the getter method to get value of the field. Complicated stuff, really. Not surprising how it's avoided.
			try {
			PropertyDescriptor pd = new PropertyDescriptor(fields[i].getName(), object.getClass());
			Method getter = pd.getReadMethod();
			value = getter.invoke(object);
			}catch(Exception e) {
				e.printStackTrace();
			}	
			stringBuild.append(value);
			if(i+1<fields.length) stringBuild.append("',");
			else stringBuild.append("');");
		}
		
		String finalStatement = new String(stringBuild);
		
		ps.executeUpdate(finalStatement);
	}
	
	public JTable findById(Object obj, JTable table) throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		//Get the getId method for this object
		PropertyDescriptor pd = new PropertyDescriptor("id", obj.getClass());
		int id = (int) pd.getReadMethod().invoke(obj);
		
		String tableName = obj.getClass().getSimpleName().toLowerCase();
		StringBuilder state = new StringBuilder("SELECT * FROM `store`.`");
		state.append(tableName);state.append("` WHERE `");
		state.append(tableName + "`.`id` = " + id + ";");
		
		//System.out.print(state);
		
		rs = ps.executeQuery(new String(state));
		
		do {
		DbUtils.resultSetToTableModel(table, rs);
		}while(rs.next());
		
		return table;
	}
	
	public void deleteAtId(Object obj) throws SQLException, IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		//Get the getId method for this object
		PropertyDescriptor pd = new PropertyDescriptor("id", obj.getClass());
		int id = (int) pd.getReadMethod().invoke(obj);
		
		String tableName = obj.getClass().getSimpleName().toLowerCase();
		StringBuilder state = new StringBuilder("DELETE FROM `store`.`");
		state.append(tableName);state.append("` WHERE `");
		state.append(tableName + "`.`id` = " + id + ";");	
		
		ps.executeUpdate(new String(state));
	}
	
	public void updateAtId(Object obj) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException, SQLException {
		//Get the getId method for this object
		PropertyDescriptor pd = new PropertyDescriptor("id", obj.getClass());
		int id = (int) pd.getReadMethod().invoke(obj);
		
		String tableName = obj.getClass().getSimpleName().toLowerCase();
		StringBuilder state = new StringBuilder("UPDATE `store`.`");
		state.append(tableName); state.append("` SET ");
		
		Field[] fields = obj.getClass().getDeclaredFields(); //get fields to know what kind of object we're changing
		
		boolean itsOkToUseMe = true;
		
		for(int i = 0; i < fields.length; i++) {
			Object value = null;
			//Use the getter method to get value of the field. Complicated stuff, really. Not surprising how it's avoided.
			try {
			pd = new PropertyDescriptor(fields[i].getName(), obj.getClass());
			Method getter = pd.getReadMethod();
			value = getter.invoke(obj);
			}catch(Exception e) {
				e.printStackTrace();
			}
			if(value != null && !(value.toString().equals("")) && !(fields[i].getName().toString().equals("id"))) {
				if(itsOkToUseMe) {itsOkToUseMe = false;}
				else {state.append(",");}
				state.append("`" + fields[i].getName() + "`='");
				state.append(value + "'");
			}
		}
		
		
		
		
		state.append(" WHERE `");
		state.append(tableName + "`.`id` = " + id + ";");
		
		ps.executeUpdate(new String(state));
	}

	public void populateTable(JTable table) throws SQLException
	{
		rs = ps.executeQuery("SELECT * FROM `store`.`" + table.getName().toLowerCase() + "`;");

		do
		{
			DbUtils.resultSetToTableModel(table, rs);
		}while(rs.next());		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void getComboModel(String objectType, JComboBox box) throws SQLException {
	    DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
	    ResultSet rs = ps.executeQuery("select name from `store`.`" + objectType + "`;");
	    while (rs.next()) {
	    	String c = rs.getString("name");
	        model.addElement(c);
	    }
	    box.setModel(model);
	}
}
