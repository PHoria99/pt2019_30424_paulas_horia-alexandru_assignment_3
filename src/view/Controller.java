package view;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;
import businessLayer.*;
import dataAccess.*;
import model.Customer;
import model.Order;
import model.Product;

public class Controller implements ActionListener{
	private JTextField customerName, customerID, customerAddress;
	private JTextField productName, productID, productQuantity, productPrice;
	private JTextField orderQuantity, orderID;
	private JComboBox<?> orderCustomer, orderProduct;
	private JButton addCustomer, addOrder, addProduct;
	private JButton removeCustomer, removeProduct, removeOrder;
	private JButton updateCustomer, updateProduct;
	private JButton findCustomer, findOrder, findProduct;
	private JTable table1, table2, table3;
	
	private DataAccess da = new DataAccess();
	
	Controller(	JTextField customerName, JTextField customerID, JTextField customerAddress,
				JTextField productName, JTextField productID, JTextField productQuantity, JTextField productPrice,
				JTextField orderQuantity, JTextField orderID,
				JComboBox<?> orderCustomer, JComboBox<?> orderProduct,
				JButton addCustomer,JButton addOrder, JButton addProduct,
				JButton removeCustomer, JButton removeProduct, JButton removeOrder,
				JButton updateCustomer, JButton updateProduct,
				JTable table1, JTable table2, JTable table3,
				JButton findCustomer, JButton findOrder, JButton findProduct)throws SQLException{
		this.customerName=customerName; this.customerID=customerID; this.customerAddress=customerAddress;
		this.productName=productName; this.productID=productID; this.productQuantity=productQuantity; this.productPrice=productPrice;
		this.orderQuantity=orderQuantity; this.orderID = orderID;
		this.orderCustomer=orderCustomer; this.orderProduct=orderProduct;
		this.addCustomer=addCustomer; this.addOrder=addOrder; this.addProduct=addProduct;
		this.removeCustomer=removeCustomer; this.removeProduct=removeProduct; this.removeOrder = removeOrder;
		this.updateCustomer=updateCustomer; this.updateProduct=updateProduct;
		this.table1=table1; this.table2=table2; this.table3=table3;
		this.findCustomer = findCustomer; this.findOrder = findOrder; this.findProduct = findProduct;
	}

	@Override
	public void actionPerformed(ActionEvent e){
		JButton b = (JButton) e.getSource();
		//CREATE
		if(b == addCustomer) {
			Customer t = new Customer(Integer.parseInt(customerID.getText()), customerName.getText(), customerAddress.getText());
			try {
				da.insertElement(t);
				populateTables();
				populateCombos();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}if(b == addProduct) {
			Product t = new Product(Integer.parseInt(productID.getText()), productName.getText(), productPrice.getText(), productQuantity.getText());
			try {
				da.insertElement(t);
				populateTables();
				populateCombos();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}if(b == addOrder) {
			Order t = new Order(Integer.parseInt(orderID.getText()), this.orderCustomer.getSelectedItem().toString(), this.orderProduct.getSelectedItem().toString(), this.orderQuantity.getText());
			try {
				da.insertElement(t);
				populateTables();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		//Read
		if(b == findCustomer) {
			if(customerID.getText().equals(""))
				try {
					da.populateTable(this.table1);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			else {
				Customer t = new Customer();t.setId(Integer.parseInt(customerID.getText()));
				try {
					da.findById(t, table1);
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IntrospectionException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}if(b == findProduct) {
			if(productID.getText().equals(""))
				try {
					da.populateTable(this.table2);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			else {
				Product t = new Product();t.setID(Integer.parseInt(productID.getText()));
				try {
					da.findById(t, table2);
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IntrospectionException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}if(b == findOrder) {
			if(orderID.getText().equals(""))
				try {
					da.populateTable(this.table3);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			else {
				Order t = new Order();t.setId(Integer.parseInt(orderID.getText()));
				try {
					da.findById(t, table3);
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IntrospectionException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		
		//UPDATE
		if(b == updateCustomer) {
			Customer t = new Customer(Integer.parseInt(customerID.getText()), customerName.getText(), customerAddress.getText());
			try {
				da.updateAtId(t);
				populateTables();
				populateCombos();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IllegalAccessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IllegalArgumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InvocationTargetException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IntrospectionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}if(b == updateProduct) {
			Product t = new Product(Integer.parseInt(productID.getText()), productName.getText(), productPrice.getText(), productQuantity.getText());
			try {
				da.updateAtId(t);
				populateTables();
				populateCombos();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IllegalAccessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IllegalArgumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InvocationTargetException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IntrospectionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		//DELETE
		if(b == removeCustomer) {
			Customer t = new Customer(); t.setId(Integer.parseInt(customerID.getText()));
			try {
				da.deleteAtId(t);
				populateTables();
				populateCombos();
			} catch (SQLException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IntrospectionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}if(b == removeProduct) {
			Product t = new Product(); t.setID(Integer.parseInt(productID.getText()));
			try {
				da.deleteAtId(t);
				populateTables();
				populateCombos();
			} catch (SQLException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IntrospectionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		if(b == removeOrder) {
			Order t = new Order(); t.setId(Integer.parseInt(orderID.getText()));
			try {
				da.deleteAtId(t);
				populateTables();
				populateCombos();
			} catch (SQLException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IntrospectionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	private void populateTables() throws SQLException
	{
		da.populateTable(this.table1);
		da.populateTable(this.table2);
		da.populateTable(this.table3);
	}
	
	private void populateCombos() throws SQLException
	{
		da.getComboModel("customer", this.orderCustomer);
		da.getComboModel("product", this.orderProduct);
	}
	
}
