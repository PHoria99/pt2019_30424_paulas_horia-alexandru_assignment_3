package view;
import java.awt.FlowLayout;
import java.sql.SQLException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import dataAccess.*;
public class GUI {
	private JFrame frame;
	private JTabbedPane panel;
	private JTable table1, table2, table3;
	private JButton addCustomer, addOrder, addProduct;
	private JButton removeCustomer, removeProduct, removeOrder;
	private JButton findCustomer, findOrder, findProduct;
	private JButton updateCustomer, updateProduct;
	private JPanel pane1, pane2, pane3;
	private JScrollPane pane11, pane21, pane31;
	private JPanel pane12, pane22, pane32;
	private JTextField customerName, customerID, customerAddress;
	private JTextField productName, productID, productQuantity, productPrice;
	private JTextField orderQuantity, orderID;
	@SuppressWarnings("rawtypes")
	private JComboBox orderCustomer, orderProduct;
	private JLabel cName, cID, cAddr, pName, pID, pQuan, oQuan, oCust, oProd, pPrice;
	
	private DataAccess da = new DataAccess();
	private Controller control;
	
	
	GUI() throws SQLException
	{
		this.initialize();
	}

	@SuppressWarnings("rawtypes")
	private void initialize() throws SQLException
	{
		this.frame = new JFrame();
		this.frame.setSize(800, 640);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.panel = new JTabbedPane();
		//each tabbed pane
		this.pane1 = new JPanel();
		this.pane1.setLayout(new BoxLayout(this.pane1, BoxLayout.PAGE_AXIS));
		this.pane2 = new JPanel();
		this.pane2.setLayout(new BoxLayout(this.pane2, BoxLayout.PAGE_AXIS));
		this.pane3 = new JPanel();
		this.pane3.setLayout(new BoxLayout(this.pane3, BoxLayout.PAGE_AXIS));
		
		//Panels for tabbed pane
		this.pane11 = new JScrollPane();
		this.pane21 = new JScrollPane();
		this.pane31 = new JScrollPane();
		this.pane12 = new JPanel();
		this.pane22 = new JPanel();
		this.pane32 = new JPanel();
		
		this.pane12.setLayout(new FlowLayout());
		this.pane22.setLayout(new FlowLayout()); 
		this.pane32.setLayout(new FlowLayout());
		
		
		this.table1 = new JTable();
		this.table1.setName("customer");
		this.table1.setFillsViewportHeight(true);
		this.table2 = new JTable();
		this.table2.setName("product");
		this.table2.setFillsViewportHeight(true);
		this.table3 = new JTable();
		this.table3.setName("order");
		this.table3.setFillsViewportHeight(true);
				
		this.pane11.setViewportView(this.table1);
		this.pane21.setViewportView(this.table2);
		this.pane31.setViewportView(this.table3);
		
		//BUTTONS
		this.addCustomer = new JButton("ADD");
		this.removeCustomer = new JButton("DELETE");
		this.updateCustomer = new JButton("UPDATE");
		this.findCustomer = new JButton("FIND");
		
		this.addProduct = new JButton("ADD");
		this.removeProduct = new JButton("DELETE");
		this.updateProduct = new JButton("UPDATE");
		this.findProduct = new JButton("FIND");
		
		this.addOrder = new JButton("ORDER");
		this.removeOrder = new JButton("DELETE");
		this.findOrder = new JButton("FIND");
		
		//TextFields and ComboBoxes
		this.customerID=new JTextField();
		this.customerID.setColumns(3);
		this.customerName=new JTextField();
		this.customerName.setColumns(16);
		this.customerAddress=new JTextField();
		this.customerAddress.setColumns(16);
		
		this.productID = new JTextField();
		this.productID.setColumns(3);
		this.productName = new JTextField();
		this.productName.setColumns(12);
		this.productPrice = new JTextField();
		this.productPrice.setColumns(3);
		this.productQuantity = new JTextField();
		this.productQuantity.setColumns(16);
		
		this.orderID= new JTextField();
		this.orderID.setColumns(3);
		this.orderCustomer = new JComboBox();
		this.orderProduct = new JComboBox();
		this.orderQuantity = new JTextField();
		this.orderQuantity.setColumns(5);
		
		//Labels UGH
		this.cName = new JLabel("Name:");
		this.cID = new JLabel("ID:");
		this.cAddr = new JLabel("Address:");
		this.pName = new JLabel("Name:");
		this.pID = new JLabel("ID:");
		this.pPrice = new JLabel("Price:");
		this.pQuan = new JLabel("Quantity:");
		this.oQuan = new JLabel("Quantity:");
		this.oCust = new JLabel("Customer:");
		this.oProd = new JLabel("Product:");
		
		
		
//*********************************************************************************ADD COMPONENTS
		this.pane12.add(this.cID);
		this.pane12.add(this.customerID);
		this.pane12.add(this.cName);
		this.pane12.add(this.customerName);
		this.pane12.add(this.cAddr);
		this.pane12.add(this.customerAddress);
		
		this.pane22.add(this.pID);
		this.pane22.add(this.productID);
		this.pane22.add(this.pName);
		this.pane22.add(this.productName);
		this.pane22.add(pPrice);
		this.pane22.add(productPrice);
		this.pane22.add(this.pQuan);
		this.pane22.add(this.productQuantity);
		
		this.pane32.add(this.orderID);
		this.pane32.add(this.oCust);
		this.pane32.add(this.orderCustomer);
		this.pane32.add(this.oProd);
		this.pane32.add(this.orderProduct);
		this.pane32.add(this.oQuan);
		this.pane32.add(this.orderQuantity);
		
		this.pane12.add(this.addCustomer);
		this.pane12.add(this.removeCustomer);
		this.pane12.add(this.updateCustomer);
		this.pane12.add(this.findCustomer);
		
		this.pane22.add(this.addProduct);
		this.pane22.add(this.removeProduct);
		this.pane22.add(this.updateProduct);
		this.pane22.add(this.findProduct);
		
		this.pane32.add(this.addOrder);
		this.pane32.add(this.removeOrder);
		this.pane32.add(this.findOrder);
		
		this.pane1.add(this.pane11);
		this.pane1.add(this.pane12);
		
		this.pane2.add(this.pane21);
		this.pane2.add(this.pane22);
		
		this.pane3.add(this.pane31);
		this.pane3.add(this.pane32);
//*********************************************************************************************************************
		
		this.panel.addTab("Customers", this.pane1);
		this.panel.addTab("Products", this.pane2);
		this.panel.addTab("Orders", this.pane3);
		
		this.frame.add(this.panel);
		
		this.frame.setVisible(true);
		
		try {
			this.populateTables();
			this.populateCombos();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		
		control = new Controller(this.customerName, this.customerID, this.customerAddress,
				this.productName, this.productID, this.productQuantity, this.productPrice,
				this.orderQuantity, this.orderID,
				this.orderCustomer, this.orderProduct,
				this.addCustomer,this.addOrder, this.addProduct,
				this.removeCustomer, this.removeProduct, this.removeOrder,
				this.updateCustomer, this.updateProduct,
				this.table1, this.table2, this.table3,
				this.findCustomer, this.findOrder, this.findProduct);
		
		this.addCustomer.addActionListener(control);
		this.addProduct.addActionListener(control);
		this.addOrder.addActionListener(control);
		this.removeCustomer.addActionListener(control);
		this.removeProduct.addActionListener(control);
		this.removeOrder.addActionListener(control);
		this.updateCustomer.addActionListener(control);
		this.updateProduct.addActionListener(control);
		this.findCustomer.addActionListener(control);
		this.findOrder.addActionListener(control);
		this.findProduct.addActionListener(control);
	}
	
	private void populateTables() throws SQLException
	{
		da.populateTable(this.table1);
		da.populateTable(this.table2);
		da.populateTable(this.table3);
	}
	
	private void populateCombos() throws SQLException
	{
		da.getComboModel("customer", this.orderCustomer);
		da.getComboModel("product", this.orderProduct);
	}
}
