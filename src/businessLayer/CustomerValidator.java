package businessLayer;
import java.util.regex.Pattern;



public class CustomerValidator{
	public static void validate(String cN, String cA)
	{
		validateName(cN);
		validateAddress(cA);
	}
	
	private static void validateName(String c) {
		String namePattern = new String("^[a-zA-Z\\s]*$");
		Pattern pattern = Pattern.compile(namePattern);
		
		if(!pattern.matcher(c).matches()) {
			throw new IllegalArgumentException("Name is not valid");
		}
	}
	
	private static void validateAddress(String c) {
		String addressPattern = new String("^[a-zA-Z0-9,.\\s]*$");
		Pattern pattern = Pattern.compile(addressPattern);
		
		if(!pattern.matcher(c).matches()) {
			throw new IllegalArgumentException("Address is not valid");
		}
	}
	
}
