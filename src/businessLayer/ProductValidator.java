package businessLayer;
import java.util.regex.Pattern;

public class ProductValidator {
	public static void validate(String pN, String pQ, String pP) {
		validateName(pN);
		validateQuantity(pQ);
		validatePrice(pN);
	}
	
	private static void validateName(String p) {
		String namePattern = new String("^[a-zA-Z\\s]*$");
		Pattern pattern = Pattern.compile(namePattern);
		
		if(!pattern.matcher(p).matches()) {
			throw new IllegalArgumentException("Name is not valid");
		}
	}
	
	private static void validateQuantity(String p) {
		String namePattern = new String("^[0-9]+$");
		Pattern pattern = Pattern.compile(namePattern);
		
		if(!pattern.matcher(p).matches()) {
			throw new IllegalArgumentException("Quantity is not valid");
		}
	}
	
	private static void validatePrice(String p) {
		String namePattern = new String("^[0-9]+$");
		Pattern pattern = Pattern.compile(namePattern);
		
		if(!pattern.matcher(p).matches()) {
			throw new IllegalArgumentException("Price is not valid");
		}
	}
}
