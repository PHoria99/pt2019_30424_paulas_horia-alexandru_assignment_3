package businessLayer;
import java.util.regex.Pattern;

public class OrderValidator {
	public static void validate(String o)
	{
		String namePattern = new String("^[0-9]+$");
		Pattern pattern = Pattern.compile(namePattern);
		
		if(!pattern.matcher(o).matches()) {
			throw new IllegalArgumentException("Quantity is not valid");
		}
	}
}
